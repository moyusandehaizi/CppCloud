package tool

import (
	"fmt"
	"strconv"
)

// HoMap 泛型map，主要用于处理json的object
type HoMap map[string]interface{}
type HoList []interface{}

// Conver2String 转化任意类型到string
func Conver2String(obj interface{}) (str string, ok bool) {
	ok = true
	switch obj.(type) {
	case string:
		str = obj.(string)
	case int:
		str = strconv.Itoa(obj.(int))
	case bool:
		str = strconv.FormatBool(obj.(bool))
	case int64:
		str = strconv.FormatInt(obj.(int64), 10)
	case float32:
		str = strconv.FormatFloat(float64(obj.(float32)), 'f', 6, 64)
	case float64:
		str = strconv.FormatFloat(obj.(float64), 'f', 6, 64)

	default:
		str = ""
		ok = false
	}
	return
}

// Conver2Int 转化任意类型到int
func Conver2Int(obj interface{}) (val int, ok bool) {
	ok = true
	val = 0
	switch obj.(type) {
	case int:
		val = obj.(int)
	case bool:
		if obj.(bool) {
			val = 1
		}
	case int64:
		val = int(obj.(int64))
	case float32:
		val = int(obj.(float32))
	case float64:
		val = int(obj.(float64))
	case string:
		var err error
		val, err = strconv.Atoi(obj.(string))
		ok = (nil == err)

	default:
		ok = false
	}
	return
}

// Conver2Float64 转化任意类型到float64
func Conver2Float64(obj interface{}) (val float64, ok bool) {
	ok = true
	val = 0
	switch obj.(type) {
	case float64:
		val = obj.(float64)
	case float32:
		val = float64(obj.(float32))
	case int:
		val = float64(obj.(int))
	case bool:
		if obj.(bool) {
			val = 1
		}
	case int64:
		val = float64(obj.(int64))
	case string:
		var err error
		val, err = strconv.ParseFloat(obj.(string), 64)
		ok = (nil == err)

	default:
		ok = false
	}
	return
}

// GetString 返回对应值的string
func (hmp HoMap) GetString(key string) (string, bool) {
	if val0, ok := hmp[key]; ok {
		if val1, ok1 := val0.(string); ok1 {
			return val1, true
		}

		return fmt.Sprint(val0), true
	}
	return "", false
}

// GetInt 返回对应值的int
func (hmp HoMap) GetInt(key string) (int, bool) {
	if val0, ok := hmp[key]; ok {
		if val1, ok1 := val0.(int); ok1 {
			return val1, true
		}

		return Conver2Int(val0)
	}
	return 0, false
}

// GetFloat64 返回对应值的float64
func (hmp HoMap) GetFloat64(key string) (float64, bool) {
	if val0, ok := hmp[key]; ok {
		if val1, ok1 := val0.(float64); ok1 {
			return val1, true
		}

		return Conver2Float64(val0)
	}
	return 0, false
}

// GetHoMap 返回map形式对象
func (hmp HoMap) GetHoMap(key string) (retmp HoMap, ok bool) {
	if val0, ok0 := hmp[key]; ok0 {
		if val1, ok1 := val0.(HoMap); ok1 {
			return val1, true
		}
	}
	return nil, false
}

// GetList map里面返回key对应值（值是数组形式）
func (hmp HoMap) GetList(key string) (list HoList, ok bool) {
	if val0, ok0 := hmp[key]; ok0 {
		if val1, ok1 := val0.(HoList); ok1 {
			return val1, true
		}
	}
	return nil, false
}

// GetString 获取Holist中某一索引处的字符串值
func (hli HoList) GetString(index int) (val string, ok bool) {
	if len(hli) > index {
		val0 := hli[index]
		return Conver2String(val0)
	}
	return "", false
}

// GetInt 获取Holist中某一索引处的字符串值
func (hli HoList) GetInt(index int) (val int, ok bool) {
	if len(hli) > index {
		val0 := hli[index]
		return Conver2Int(val0)
	}
	return 0, false
}

// GetFloat64 获取Holist中某一索引处的字符串值
func (hli HoList) GetFloat64(index int) (val float64, ok bool) {
	if len(hli) > index {
		val0 := hli[index]
		return Conver2Float64(val0)
	}
	return 0, false
}
